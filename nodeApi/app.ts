import express, {Express, Request, Response} from 'express';
import dotenv from 'dotenv';
import { Customer } from './models/customer';
import { create, findAll } from './services/customerService';



dotenv.config();

const app: Express = express();
app.use(express.json());
const port = process.env.PORT;




app.get('/', (req: Request, res: Response) => res.send('This is a final project'));

app.get('/customer', async(req: Request, res: Response) => {
    try {
        const customers: Customer[] = await findAll();
        res.status(200).send(customers);
      } catch (e: any) {
        res.status(500).send(e.message);
      }

});



app.post("/customer", async (req: Request, res: Response) => {
    try {
      const payload: Customer = req.body;
      const customer = await create(payload);
      res.status(201).json(customer);
    } catch (e: any) {
        res.status(500).send(e.message);
    }
  });


app.listen(port, () =>{
    console.log(`I am listening on port : ${port}`)
});