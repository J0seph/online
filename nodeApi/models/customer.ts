export class Customer{
    Id: string;
    Name: string;
    LastName: string;
    CompanyJob: string;
    Phone: string;
    Email: string;

    constructor(
        id: string,
        name: string,
        lastName: string,
        companyJob: string,
        phone: string,
        email: string
    ){
        this.Id = id;
        this.Name = name;
        this.LastName = lastName;
        this.CompanyJob = companyJob;
        this.Phone = phone;
        this.Email = email;
    }
}