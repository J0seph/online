import { Customer } from "../models/customer";
let customers: Customer[] = [];

export const findAll = async (): Promise<Customer[]> => Object.values(customers);

export const create = async (customer: Customer): Promise<Customer> => {
    customers.push(customer);
    return customer;
};