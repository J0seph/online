"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const customerService_1 = require("./services/customerService");
dotenv_1.default.config();
const app = (0, express_1.default)();
app.use(express_1.default.json());
const port = process.env.PORT;
app.get('/', (req, res) => res.send('This is a final project'));
app.get('/customer', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const customers = yield (0, customerService_1.findAll)();
        res.status(200).send(customers);
    }
    catch (e) {
        res.status(500).send(e.message);
    }
}));
app.post("/customer", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = req.body;
        const customer = yield (0, customerService_1.create)(payload);
        res.status(201).json(customer);
    }
    catch (e) {
        res.status(500).send(e.message);
    }
}));
app.listen(port, () => {
    console.log(`I am listening on port : ${port}`);
});
