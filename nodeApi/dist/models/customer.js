"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = void 0;
class Customer {
    constructor(id, name, lastName, companyJob, phone, email) {
        this.Id = id;
        this.Name = name;
        this.LastName = lastName;
        this.CompanyJob = companyJob;
        this.Phone = phone;
        this.Email = email;
    }
}
exports.Customer = Customer;
