package com.online.ups.joseph.online.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.modelmapper.ModelMapper;

import com.online.ups.joseph.online.model.Customer;

public class CustomerProcess implements Processor{
    
    private ModelMapper modelMapper = new ModelMapper();
    @Override
    public void process(Exchange exchange) throws Exception{
        Customer customer = modelMapper.map(exchange.getMessage().getBody(), Customer.class);
        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/json");
        String channel = customer.getChannel().toLowerCase();
        exchange.getIn().setHeader("channel", channel);
        exchange.getIn().setBody(customer);
    }    
}
