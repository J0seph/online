package com.online.ups.joseph.online.camel.route;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;
import com.online.ups.joseph.online.camel.processor.*;
import com.online.ups.joseph.online.model.Customer;;

@Component
public class RestRouter extends RouteBuilder {
    
    JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Customer.class);

    @Override
    public void configure() throws Exception{

        restConfiguration().component("servlet").enableCORS(false);

        from("direct:inCustomer").process(new CustomerProcess())
        .choice()
            .when(header("channel").contains("digital"))
                .marshal(jsonDataFormat)
                .to("rest:post:/customer?host=localhost:3001")
            .otherwise()
                .marshal(jsonDataFormat)
                .to("rest:post:/customer?host=localhost:7089")
        .end();
    }
}
