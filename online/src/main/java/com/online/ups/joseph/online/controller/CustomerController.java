package com.online.ups.joseph.online.controller;

import java.util.List;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.online.ups.joseph.online.model.*;



@RestController
public class CustomerController {
    private final CustomerRepository repository;
    @Autowired private ProducerTemplate producerTemplate;

    CustomerController(CustomerRepository repository) {
        this.repository = repository;
      }

    @GetMapping("/customer")
    List<Customer> all() {
        return repository.findAll();
    }

    @PostMapping("/customer")
    Customer newCustomer(@RequestBody Customer customer) {
        repository.save(customer);
        producerTemplate.requestBody("direct:inCustomer", customer);
        return customer;
    }
}
