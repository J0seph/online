package com.online.ups.joseph.online.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Customer {
    
    private @Id @GeneratedValue Long id;
    public void setId(Long id) {
        id = id;
    }

    public Long getId() {
        return id;
    }

    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String lastName;


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String companyJob;
    

    public String getCompanyJob() {
        return companyJob;
    }

    public void setCompanyJob(String companyJob) {
        this.companyJob = companyJob;
    }

    private String phone;
    

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private String email;
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String channel;

    

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    Customer(){
        
    }

    Customer(String name, String lastName, String companyJob, String phone, String email, String channel){
        this.name = name;
        this.lastName = lastName;
        this.companyJob = companyJob;
        this.phone = phone;
        this.email = email;
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + this.id.toString() + ", name='" + this.name + '\'' + ", Company='" + this.companyJob + '\'' + '}';
    }
}
