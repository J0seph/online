CREATE TABLE Customer( 
    Id int,
    Name varchar(20),
    LastName varchar(20),
    CompanyJob varchar(20),
    Phone varchar(20),
    Email varchar(20),
    Channel varchar(20)
);