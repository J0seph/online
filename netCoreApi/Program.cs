var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
    {
        builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
    }));
    


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("corsapp");
app.UseHttpsRedirection();

List<Customer> customers = new List<Customer>();

app.MapGet("/customer", () =>
{
    return customers;
});

app.MapGet("/customer/{id}", () =>
{
    return customers;
});

app.MapPost("/customer", (Customer customer) =>
{
    customers.Add(customer);
    Console.WriteLine(customer);
    return customer;
});

app.Run();