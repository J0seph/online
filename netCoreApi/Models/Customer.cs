public class Customer
{
    public long Id {get; set;}
    public string Name {get; set;}
    public string LastName {get; set;}
    public string CompanyJob {get; set;}
    public string Phone {get; set;}
    public string Email {get; set;}
    public override string ToString()
    {
        return "Customer{" + "id=" + this.Id.ToString() + ", name='" + this.Name + '\'' + ", Company='" + this.CompanyJob + '\'' + '}';
    }
}